# Security Policy

## Supported Versions

| Version | Supported          |
| ------- | ------------------ |
| v1.3.x | ✅ |
| v1.2.x | ✅ |
| v1.1.x | ✅ |
| v1.0.x | ✅ |

## Reporting a Vulnerability

All vulnerabilities can be submitted on the [npm page](https://npmjs.org/package/@quantalabs/epijs).

## Fixes

If fixing a vulnerability, fork the repo and commit the fix to the main branch and submit a PR, and a new version will be sent out to all versions, meaning v1.1.x releases, v1.0.x, v1.3.x, or any other release. If the fix is only for a certain version, then commit it to the `maintenance/vX.X.X` branch, with `vX.X.X` being your version.
