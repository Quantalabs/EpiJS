---
home: true
heroImage: https://i.ibb.co/HKt7Bn3/Logo-Makr-4sk-LGO.png
actionText: Get Started →
actionLink: /getting-started.md
features:
    - title: Customized SIR-Based Models
    - title: Quick, readable graphs
    - title: Complex, yet simple
    - details: Easy, customizable SIR based models, with SEIR, SEIRD, SIR, and more models for you to choose from.
    - details: Quick and readable graphs made with chart.js to get the info you need, when you need it.
    - details: Complex models like SEIHCRD models done simply, in only 1-line of code.
footer: GPL-3.0 Licensed | Copyright © 2021-present Quantalabs
---

<div align='center'>
<h2 >Don't take it from us, see for yourself!</h2>
<a href="/getting-started.html">Get Started →</a>
</div>
<br>
<br>
<br>

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://github.com/Quantalabs/epijs/issues). You can also take a look at the [contributing guide](https://github.com/Quantalabs/epijs/blob/master/CONTRIBUTING.md).

## Related work

- [epispot](https://github.com/epispot/epispot)
- [CovaSim](https://covasim.org)

### Cite EpiJS in your research!
> QLabs. (2021, June 7). Quantalabs/EpiJS: v1.1.0 (Version v1.1.0). Zenodo. http://doi.org/10.5281/zenodo.4908921