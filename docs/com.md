---
title: EpiJS Module - Com
---
<!-- Generated by documentation.js. Update this documentation by updating the source code. -->

### Table of Contents


*   [Virus][2]
    *   [Parameters][3]
    *   [Examples][4]
*   [Community][5]
    *   [Parameters][6]
    *   [Examples][7]
    *   [sir][8]
        *   [Parameters][9]
        *   [Examples][10]
    *   [seir][11]
        *   [Parameters][12]
        *   [Examples][13]
    *   [seird][14]
        *   [Parameters][15]
        *   [Examples][16]
*   [compare][17]
    *   [Parameters][18]
    *   [Examples][19]

# Com

EpiJS module for representing communities

Import it with:
```javascript
       const com = require('@quantalabs/epijs').com
```
## Virus

Class representing a virus, which can infect a community.

### Parameters

*   `rnaught` **[Number][20]** The disease's R-Naught
*   `u` **[Number][20]** The disease's recovery rate
*   `a` **[Number][20]** The disease's incubation period (optional, default `0`)
*   `d` **[Number][20]** The disease's death rate (for infected population) (optional, default `0`)
*   `h` **[Number][20]** The disease's hospitalization rate
*   `dh` **[Number][20]** The disease's death rate (for hospitalized population)

### Examples

```javascript
let covid = new Virus(5.7, 2.1/100)
```

## Community

Class representing a community, which can be infected with a disease, and compared to other communities.

### Parameters

*   `pop` **[Number][20]** The population of the community
*   `i` **[Number][20]** The start infected population of the community.
*   `s` **[Number][20]** The start susceptible population of the community

### Examples

```javascript
let NewYorkCity = new Community(8419000, 300, 8418700)
```

### sir

SIR model for the community

#### Parameters

*   `disease` **[Number][20]** A virus class. The virus to infect the community with and model for.
*   `time` **[Number][20]** Time to predict for.

#### Examples

```javascript
let NewYorkCity = new Community(8419000, 300, 8418700)
let covid = new Virus(5.7, 2.1/100)

outbreak = NewYorkCity.sir(covid, 100)
```

### seir

SEIR model for the community

#### Parameters

*   `disease` **[Number][20]** A virus class. The virus to infect the community with and model for.
*   `time` **[Number][20]** Time to predict for.

#### Examples

```javascript
let NewYorkCity = new Community(8419000, 300, 8418700)
let covid = new Virus(5.7, 2.1/100, a=1/8)

outbreak = NewYorkCity.seir(covid, 100)
```

### seird

SEIRD model for the community

#### Parameters

*   `disease` **[Number][20]** A virus class. The virus to infect the community with and model for.
*   `time` **[Number][20]** Time to predict for.

#### Examples

```javascript
let NewYorkCity = new Community(8419000, 300, 8418700)
let covid = new Virus(5.7, 2.1/100, a=1/8, d=1/100)

outbreak = NewYorkCity.seird(covid, 100)
```

## compare

Compare's two different outbreaks, communities, or anything else.

### Parameters

*   `c` **[HTMLCanvasElement][21]** The canvas element
*   `model1` **[Function][22]** The first model to compare.
*   `model2` **[Function][22]** The second model to compare.
*   `m1name` **[String][23]** The name of the first model
*   `m2name` **[String][23]** The name of the second model
*   `days` **[Number][20]** The total amount of days to compare for.

### Examples

```javascript
let NewYorkCity = new Community(8419000, 300, 8418700)
let covid = new Virus(5.7, 2.1/100)
let covid_variant = new Virus(5, 4/100)

let chart = document.getElementById('model')

covid_outbreak = NewYorkCity.sir(covid, 100)
variant_outbreak = NewYorkCity.sir(covid_variant, 100)

compare(chart, covid_outbreak, variant_outbreak, "COVID-19", "COVID-19 Variant", 100) // We chose 100 as the amount of days, but it could be 50 or 25, not the lenght of the prediction that was in the model.
```

[1]: #chart

[2]: #virus

[3]: #parameters

[4]: #examples

[5]: #community

[6]: #parameters-1

[7]: #examples-1

[8]: #sir

[9]: #parameters-2

[10]: #examples-2

[11]: #seir

[12]: #parameters-3

[13]: #examples-3

[14]: #seird

[15]: #parameters-4

[16]: #examples-4

[17]: #compare

[18]: #parameters-5

[19]: #examples-5

[20]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number

[21]: https://developer.mozilla.org/docs/Web/API/HTMLCanvasElement

[22]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/function

[23]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String
