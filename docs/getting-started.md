---
    title: EpiJS - Getting Started
---

# Getting Started

Getting started with EpiJS

## Install

```sh
npm install epijs
```
Or use it in your webpage:
```HTML
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.2/dist/chart.min.js"></script> <!-- Chart.js is required. -->
<script src="https://cdn.jsdelivr.net/npm/mathjs@9.4.2/lib/browser/math.min.js"></script> <!-- Math.js is required -->
<script src="https://cdn.jsdelivr.net/gh/Quantalabs/EpiJS/web/index.min.js"></script>
```

## Metadata

You can fetch metadata about the package with the following commands (this is only with the NPM Package, unfortunatley):
```js
epijs.about // General metadata
epijs.version // Your current package version
```

