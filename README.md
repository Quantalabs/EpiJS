<div align="center"><img src="https://dev-to-uploads.s3.amazonaws.com/uploads/articles/meve5c0a4lky6b6veif9.png" alt="EpiJS Logo"></div>

---

<p align="center">
  <a href="https://github.com/Quantalabs/epijs/graphs/commit-activity" target="_blank">
    <img alt="Maintenance" src="https://img.shields.io/badge/Maintained%3F-yes-green.svg?style=flat-square" />
  </a>
  <a href="https://github.com/Quantalabs/epijs/blob/main/LICENSE" target="_blank">
    <img alt="License: GPL--3.0" src="https://img.shields.io/github/license/Quantalabs/EpiJS?style=flat-square" />
  </a>
  <a href="https://npmjs.org/package/@quantalabs/epijs" target="_blank">
    <img alt="npm version" src="https://img.shields.io/npm/v/@quantalabs/epijs?style=flat-square">
    <img alt="npm downloads" src="https://img.shields.io/npm/dt/@quantalabs/epijs?color=%232c5fde&label=npm%20downloads&style=flat-square">
  </a>
  <a href="https://github.com/Quantalabs/epijs/issues/" target="_blank">
    <img alt="GitHub issues" src="https://img.shields.io/github/issues/quantalabs/epijs?style=flat-square">
  </a>
  <a href="https://github.com/Quantalabs/EpiJS/pulls" target="_blank">
    <img alt="GitHub pull requests" src="https://img.shields.io/github/issues-pr/Quantalabs/epijs?style=flat-square">
  </a>
  <a href="https://quantalabs.github.io/EpiJS/">
    <img alt="GitHub Pages deployments" src="https://img.shields.io/github/deployments/quantalabs/epijs/github-pages?label=GitHub%20Pages%20Deployment&style=flat-square">
  </a>
  <blockquote align="center">
  <p>A disease modeling package.</p>
  </blockquote>
  <h4 id="-homepage-https-github-com-quantalabs-epijs-readme-" align="center">🏠 <a href="https://github.com/Quantalabs/epijs#readme">Homepage</a></h4>
  <h4 id="-demo-https-npm-runkit-com-epijs-" align="center">✨ <a href="https://epi.js.org/demo/index.html">Demo</a></h4>
</p>

## Install

```sh
npm install @quantalabs/epijs
```

## Usage

```js
const epijs = require("@quantalabs/epijs")
```

Or in your website:
```html
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.2/dist/chart.min.js"></script> <!-- Chart.js is required. -->
<script src="https://cdn.jsdelivr.net/npm/mathjs@9.4.2/lib/browser/math.min.js"></script> <!-- Math.js is required -->
<script src="https://cdn.jsdelivr.net/gh/Quantalabs/EpiJS/web/index.min.js"></script>
```

Or get the latest version from git:

```sh
git clone https://github.com/Quantalabs/epijs.git 
cd EpiJS
npm install
```

## Screenshots

SIR Model: \
![image](https://user-images.githubusercontent.com/55121845/110269885-81c6ec80-7f79-11eb-98bd-9964db28d371.png) \
SEIR Model: \
![SEIR Model graph](https://user-images.githubusercontent.com/55121845/110269735-2e549e80-7f79-11eb-862e-d248533f0bc1.jpg) \
SEIRD Model: \
![SEIRD Model graph](https://user-images.githubusercontent.com/55121845/110269786-4b896d00-7f79-11eb-8844-6f0df85faa3f.png) \

## Author

👤 **QLabs (@Quantalabs)**

* Website: https://quantalabs.github.io/
* Github: [@Quantalabs](https://github.com/Quantalabs)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://github.com/Quantalabs/epijs/issues). You can also take a look at the [contributing guide](https://github.com/Quantalabs/epijs/blob/master/CONTRIBUTING.md).

## Show your support

Give a ⭐️ if this project helped you!

## 📝 License

Copyright © 2021 [QLabs (@Quantalabs)](https://github.com/Quantalabs).<br />
This project is [GPL-3.0](https://github.com/Quantalabs/epijs/blob/master/LICENSE) licensed.

## Related work

- [epispot](https://github.com/epispot/epispot)
- [CovaSim](https://covasim.org)

### Cite EpiJS in your research!
> QLabs. (2021, June 7). Quantalabs/EpiJS: v1.1.0 (Version v1.1.0). Zenodo. http://doi.org/10.5281/zenodo.4908921

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
