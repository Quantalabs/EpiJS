# Contributing guidelines

## Fixing Issues

Check out all [available issues](https://github.com/Quantalabs/EpiJS/issues?q=is%3Aissue+is%3Aopen+label%3A%22Status%3A+Available%22) and claim them with `/claim`. Make sure to reference these issues when you submit your PR.

## Adding features

If there's a feature request already submitted and avaliable on the [issues page](https://github.com/Quantalabs/EpiJS/issues?q=is%3Aissue+is%3Aopen+label%3Aenhancement+label%3A%22status%3A+available%22) and claim it with `/claim` in the comments. Make sure to reference these issues when you submit the PR.
